1. Soal 1 Membuat Database

create database myshop;

2. Soal 2 Membuat Table di Dalam Database

Table users
create table users(
    -> id int(30) primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );

Table categories
create table categories(
    -> id int(30) primary key auto_increment,
    -> name varchar(255)
    -> );

Table items
create table items(
    -> id int(30) primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(30),
    -> stock int(30),
    -> category_id int(30),
    -> foreign key(category_id) references categories(id)
    -> );

3. Soal 3 Memasukkan data pada table

Table users
insert into users(name, email, password) values
    -> ("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "je
nita123");

Table categories
insert into categories(name) values
    -> ("gadget"), ("cloth"), ("men"), ("women"), ("branded");

Table items
insert into items(name,description,price,stock,category_id) va
lues
    -> ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1), ("Unik
looh", "baju keren dari brand ternama", 500000, 50, 2), ("IMHO Watch", "jam tang
an anak yang jujur banget", 2000000, 10, 1);

4. Soal 4 Mengambil data dari Database

a. mengambil data users (semua field kecuali passwordnya)
select id, name, email from users;

b. mengambil data items

- mengambil data  dengan kondisi harga>1000000
select * from items where price>1000000;

- mengambil data dengan menggunakan like dengan kata kunci "uniklo"
select * from items where name like 'uniklo%';

c. Menampilkan data items join dengan kategori
select items.name, items.description, items.price, items.stock
, items.category_id, categories.name as kategori from items left join categories
 on items.category_id = categories.id;

5. Soal 5 Mengubah data dari Database
update items set price=2500000 where id=1;update items set price=2500000 where id=1;
